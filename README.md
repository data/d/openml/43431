# OpenML dataset: Amazon-Echo-Dot-2-Reviews-Dataset

https://www.openml.org/d/43431

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Since Amazon Echo Dot 2 has been the best selling Alexa product, we decided to extract the reviews posted on Amazon for this device. This particular dataset contains reviews posted in September and October 2017. The complete dataset with all the reviews from 2016 can be downloaded from DataStock - a repository of clean and structured web datasets with historical records.
Content
Given below are the data fields:

Pageurl    
Title    
Review Text    
Device Color    
User Verified    
Review Date    
Review Useful Count    
Configuration 
Rating    
Declaration Text (Example: Vine Voice, Top 100 reviewer, etc.)

Acknowledgements
This dataset has been created via PromptCloud's in-house web data extraction solution.

Inspiration
The initial set of analyses can be access here - https://goo.gl/XHVe9b.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43431) of an [OpenML dataset](https://www.openml.org/d/43431). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43431/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43431/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43431/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

